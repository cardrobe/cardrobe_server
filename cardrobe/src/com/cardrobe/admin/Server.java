package com.cardrobe.admin;

import com.cardrobe.admin.BaseAbstractVerticle;
import com.cardrobe.admin.Message;
import com.cardrobe.admin.Util;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.LogManager;

public class Server extends BaseAbstractVerticle {
   private Logger logger = null;
   private EventBus eb;
   private JsonObject config = null;
   private Integer userID;
   private String locale;
   private String userdb;
   private final DeliveryOptions deliveryOptions = (new DeliveryOptions()).setSendTimeout(5000L);
   private static final String QUERY_PARAMS = "query_params";
   private static final String PATH_PARAMS = "path_params";
   private static final String PARAM_1 = "param_1";
   private static final String CODE = "code";
   Message msg;
   private static Map<String, Boolean> apiLookup = new HashMap();
   private static Map<String, String> resourceSubHandlers;
   private static Map<String, Map<String, String>> subHandlers;

   public void stop() {
   }

   public void start() {
      this.config = this.vertx.getOrCreateContext().config();
      System.out.println("#####-#####-#####-#####-######-#####-#####-#####");
      System.out.println("#####--------------------------------------#####");
      System.out.println("#####---- CARDROBE - Find Your Car 1.0 ----#####");
      System.out.println("#####--------------------------------------#####");
      System.out.println("#####-#####-#####-#####-######-#####-#####-#####");
      System.out.println("#####------------(c)cardrobe.in------------#####");
      System.out.println("#####-#####-#####-#####-######-#####-#####-#####");

      try {
         System.setProperty("java.util.logging.config.file", this.config.getString("logfile"));
         LogManager workerDefinitions = LogManager.getLogManager();
         workerDefinitions.readConfiguration();
      } catch (IOException | SecurityException arg16) {
         System.out.println("NOTIFY SUPORT: Error in getting logging file");
         arg16.printStackTrace();
      }

      this.logger = LoggerFactory.getLogger("-L0G-");
      this.logger.info("............Server Started.Vertx Running.............");

      try {
         Class.forName("com.mysql.jdbc.Driver");
         this.mysqlClient = DriverManager.getConnection(this.config.getString("mysql_host"));
         this.logger.info("............Database connected succesfully.............");
      } catch (SQLException | ClassNotFoundException arg15) {
         this.logger.info("NOTIFY SUPORT: Error in connecting Database", arg15);
      }

      this.eb = this.vertx.eventBus();
      JsonObject workerDefinitions1 = this.config.getJsonObject("workers");
      Set workers = workerDefinitions1.fieldNames();
      Iterator path = workers.iterator();

      while(path.hasNext()) {
         String file = (String)path.next();
         DeploymentOptions sdf = (new DeploymentOptions()).setWorker(true).setInstances(workerDefinitions1.getJsonObject(file).getInteger("instances").intValue()).setConfig(this.config);
         this.vertx.deployVerticle(file, sdf);
      }

      String path1 = Server.class.getProtectionDomain().getCodeSource().getLocation().getPath();
      File file1 = new File(path1);
      SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
      String jartime = sdf1.format(Long.valueOf(file1.lastModified()));
      PreparedStatement sqlQuery = null;
      String query = "{CALL spversion(?)}";

      try {
         sqlQuery = this.mysqlClient.prepareStatement(query);
         sqlQuery.setString(1, jartime);
         sqlQuery.executeQuery();
      } catch (Exception arg14) {
         this.logger.error("Error while storing version.", arg14);
      }

      Router router = Router.router(this.vertx);
      router.route().handler(BodyHandler.create().setUploadsDirectory(this.config.getString("file_upload_vertx")));
      Route generic = router.route().handler((routingContext) -> {
         String method = routingContext.request().method().name().toLowerCase();
         routingContext.response().headers().set("Access-Control-Allow-Origin", "*");
         JsonObject params = Util.getParams(routingContext.request().path(), routingContext.request().params());
         params.put("httpMethod", method);
         String api = params.getString("apiVersion") + "." + method + "." + params.getString("resource");
         if(method.equals("options")) {
            routingContext.response().headers().set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            if(params.getString("resource").equals("uploads")) {
               routingContext.response().headers().set("Access-Control-Allow-Headers", "Content-Type");
            }

            routingContext.response().setStatusCode(200).end();
         } else {
            Map apiSubHandler = (Map)subHandlers.get(params.getString("resource"));
            String subResource = params.getJsonObject("path_params").getString("param_1");
            String subHandler = null;
            if(apiSubHandler != null && subResource != null) {
               subHandler = (String)apiSubHandler.get(subResource.toLowerCase());
            }

            String handler = subHandler == null?api:params.getString("apiVersion") + "." + method + "." + subHandler;
            params.put("apiHandler", handler);
            routingContext.put("apiParams", params);
            Boolean authRequired = (Boolean)apiLookup.get(api);
            if(authRequired == null) {
               routingContext.response().setStatusCode(404).setStatusMessage("NOT FOUND").end();
            } else {
               if(authRequired.booleanValue()) {
                  routingContext.put("api-authorized", Boolean.valueOf(true));
               }

               routingContext.next();
            }
         }
      });
      Route auth = router.route().blockingHandler((routingContext) -> {
         this.userID = Integer.valueOf(-1);
         this.locale = "en";
         this.userdb = "cardrobe360";
         if(routingContext.get("api-authorized") == null) {
            routingContext.next();
         } else {
            JsonObject apiParams = (JsonObject)routingContext.get("apiParams");
            String sessionId = apiParams.getJsonObject("query_params").getString("sid");
            Boolean sessionValid = Boolean.valueOf(false);

            try {
               PreparedStatement e = this.mysqlClient.prepareStatement("SELECT userID,locale,dbname from appsessions WHERE sessionID like ?");
               e.setString(1, sessionId);
               ResultSet resultSet = e.executeQuery();
               if(resultSet.first()) {
                  this.userID = Integer.valueOf(resultSet.getInt("userID"));
                  this.locale = resultSet.getString("locale");
                  this.userdb = resultSet.getString("dbname");
                  sessionValid = Boolean.valueOf(true);
                  resultSet.close();
               }

               this.logger.info("|" + this.userID.toString() + "|" + routingContext.request().path());
            } catch (Exception arg6) {
               this.logger.info("|" + this.userID.toString() + "|" + routingContext.request().path());
               this.logger.error("Error while validating session", arg6);
               routingContext.response().setStatusCode(401).end((new JsonObject()).put("code", Integer.valueOf(615)).encode());
               return;
            }

            if(!sessionValid.booleanValue()) {
               this.logger.info("Cannot validate session|" + this.userID.toString() + "|" + routingContext.request().path());
               routingContext.response().setStatusCode(401).end((new JsonObject()).put("code", Integer.valueOf(615)).encode());
            } else {
               routingContext.next();
            }
         }
      });
      Route api = router.route().handler((routingContext) -> {
         Long it = Long.valueOf(System.currentTimeMillis());
         this.msg = new Message(this.locale);
         JsonObject apiParams = (JsonObject)routingContext.get("apiParams");
         String apiHandler = apiParams.getString("apiHandler");
         Buffer body = routingContext.getBody();
         JsonObject messageInfo = new JsonObject();
         this.logger.info("[MSG BODY]" + body.toString() + "|apiparams:" + apiParams.toString());

         try {
            messageInfo = new JsonObject(body.toString());
         } catch (Exception arg7) {
            ;
         }

         messageInfo.put("path_params", apiParams.getJsonObject("path_params")).put("query_params", apiParams.getJsonObject("query_params"));
         messageInfo.put("userID", this.userID);
         messageInfo.put("userdb", this.userdb);
         this.logger.info("[MSG INFO]" + messageInfo.toString());
         HttpServerResponse response = routingContext.response();
         this.eb.send(apiHandler, messageInfo.toString(), this.deliveryOptions, (reply) -> {
            Long d = Long.valueOf(System.currentTimeMillis() - it.longValue());
            if(!reply.succeeded()) {
               this.logger.info("Failed Request processing time :" + apiHandler + " : " + Long.toString(d.longValue()));
               response.setStatusCode(404);
               response.end();
            } else {
               JsonObject replyBody = (JsonObject)((io.vertx.core.eventbus.Message)reply.result()).body();
               switch(replyBody.getInteger("code").intValue()) {
               case 200:
                  response.setStatusCode(200);
                  response.setStatusMessage(this.msg.getMessage("200"));
                  response.putHeader("content-type", "application/json");
                  response.end(replyBody.encode());
                  break;
               case 404:
                  response.setStatusCode(404);
                  response.setStatusMessage("NOT FOUND");
                  response.end();
                  break;
               case 500:
                  response.setStatusCode(500);
                  response.setStatusMessage(this.msg.getMessage("500"));
                  break;
               default:
                  if(replyBody.getInteger("code") != null) {
                     response.setStatusCode(400);
                     response.setStatusMessage(this.msg.getMessage(replyBody.getInteger("code").toString()));
                     response.putHeader("content-type", "application/json");
                     response.end(replyBody.encode());
                  } else {
                     response.setStatusCode(400);
                     response.setStatusMessage(this.msg.getMessage("400"));
                  }
               }

            }
         });
      });
      String ws = this.config.getString("host_ip");
      HttpServer server = this.vertx.createHttpServer();
      router.getClass();
      server.requestHandler(router::accept).listen(this.config.getInteger("port", Integer.valueOf(8080)).intValue(), ws);
   }

   static {
      apiLookup.put("1.get.brandlink", Boolean.valueOf(false));
      apiLookup.put("1.get.dealerlink", Boolean.valueOf(false));
      apiLookup.put("1.get.financierlink", Boolean.valueOf(false));
      apiLookup.put("1.get.insurerlink", Boolean.valueOf(false));
      apiLookup.put("1.get.dealerdetails", Boolean.valueOf(false));
      apiLookup.put("1.get.financierdetails", Boolean.valueOf(false));
      apiLookup.put("1.get.insurerdetails", Boolean.valueOf(false));
      apiLookup.put("1.get.adblog", Boolean.valueOf(false));
      apiLookup.put("1.get.adblogsearch", Boolean.valueOf(false));
      apiLookup.put("1.get.product", Boolean.valueOf(false));
      apiLookup.put("1.get.recommended", Boolean.valueOf(false));
      apiLookup.put("1.get.features", Boolean.valueOf(false));
      apiLookup.put("1.post.adblogcomment", Boolean.valueOf(false));
//      apiLookup.put("1.get.brandlookup", Boolean.valueOf(false));
//      apiLookup.put("1.get.brandlookup", Boolean.valueOf(false));
//      apiLookup.put("1.get.brandlookup", Boolean.valueOf(false));
      apiLookup.put("1.post.auth", Boolean.valueOf(false));
      resourceSubHandlers = new HashMap();
      resourceSubHandlers.put("xxxxx", "resource.xxxxx");
      subHandlers = new HashMap();
      subHandlers.put("resource", resourceSubHandlers);
   }
}