package com.cardrobe.admin;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class BaseAbstractVerticle extends AbstractVerticle {
	protected JsonObject config;
	protected Logger logger;
	protected EventBus eb;
	public Connection mysqlClient = null;
	protected PreparedStatement connectionKeepAliveSelect = null;
	protected int pageSize = 50;

	public void stop() {
		try {
			this.connectionKeepAliveSelect.close();
			this.mysqlClient.close();
		} catch (SQLException arg1) {
			arg1.printStackTrace();
		}

	}

	public void start() {
      this.config = this.vertx.getOrCreateContext().config();
      this.logger = LoggerFactory.getLogger("-L0G-");
      this.eb = this.vertx.eventBus();
      this.logger.info("Start Verticle : " + this.getClass().getName());

      try {
         Class.forName("com.mysql.jdbc.Driver");
         this.mysqlClient = DriverManager.getConnection(this.config.getString("mysql_host"));
      } catch (Exception arg2) {
         this.logger.error("NOTIFY SUPPORT : Error Connecting to MySQL ", arg2);
      }

      try {
         this.connectionKeepAliveSelect = this.mysqlClient.prepareStatement("SELECT 1");
      } catch (SQLException arg1) {
         this.logger.error("Prepared statement failed ", arg1);
      }

      this.vertx.setPeriodic(10600000L, (id) -> {
         try {
            this.connectionKeepAliveSelect.executeQuery();
            this.logger.info("Database heartbeat log : " + this.getClass().getName());
         } catch (SQLException arg2) {
            this.logger.error("NOTIFY SUPPORT : Database heartbeat failed:" + this.getClass().getName(), arg2);
         }

      });
   }
}