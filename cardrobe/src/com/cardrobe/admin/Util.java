package com.cardrobe.admin;

import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map.Entry;

public class Util {
	private static final String QUERY_PARAMS = "query_params";
	private static final String PATH_PARAMS = "path_params";

	public static JsonObject getParams(String path, MultiMap qp) {
		JsonObject params = new JsonObject();
		JsonObject pathElements = new JsonObject();
		ArrayList elementsList = new ArrayList(Arrays.asList(path.split("/")));
		elementsList.removeAll(Collections.singleton(""));
		params.put("apiVersion",
				elementsList.size() > 0 ? (String) elementsList.get(0) : "");
		params.put(
				"resource",
				elementsList.size() > 1 ? ((String) elementsList.get(1))
						.toLowerCase() : "");
		int parmIdx = 0;

		for (int queryParams = 2; queryParams < elementsList.size(); ++queryParams) {
			pathElements.put("param_" + Integer.toString(parmIdx++),
					(String) elementsList.get(queryParams));
		}

		JsonObject arg8 = new JsonObject();
		Iterator entries = qp.iterator();

		while (entries.hasNext()) {
			Entry entry = (Entry) entries.next();
			arg8.put((String) entry.getKey(), (String) entry.getValue());
		}

		return params.put("path_params", pathElements)
				.put("query_params", arg8);
	}

	static double getValue(double val) {
		return val;
	}

	static double getValue(Object val) {
		return 0.0D;
	}
}