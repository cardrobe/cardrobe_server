package com.cardrobe.admin;

import com.cardrobe.admin.BaseAbstractVerticle;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ADGeneral extends BaseAbstractVerticle {
	private PreparedStatement miscQuery = null;
	private ResultSet resultSet = null;
	private static final String CODE = "code";
	private static final String RESULT = "result";
	private static final String USER_ID = "userID";
	private Integer userID;
	private static final String MODULE_ID = "moid";

	public void stop() {
	}

	public void start() {
      super.start();
      this.logger = LoggerFactory.getLogger(this.getClass());      
      this.eb.consumer("1.get.brandlink").handler(message -> {    	  
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          try {
              this.miscQuery = this.mysqlClient.prepareStatement("Select idtbadbrand,name,website from tbadbrand Where active=1 ");
              this.resultSet = this.miscQuery.executeQuery();
              ResultSetMetaData rsMetaData = this.resultSet.getMetaData();
              int numberOfColumns = rsMetaData.getColumnCount();
              while (this.resultSet.next()) {
                  JsonObject result = new JsonObject();
                  for (int i = 1; i <= numberOfColumns; ++i) {
                      result.put(rsMetaData.getColumnLabel(i), this.resultSet.getString(rsMetaData.getColumnLabel(i)));
                  }
                  resultArray.add(result);
              }
              response.put("code", Integer.valueOf(200));
              response.put("result", resultArray);
              message.reply((Object)response);
          }
          catch (Exception e) {
              this.logger.error((Object)"NOTIFY SUPPORT : Error in getting Brand Link info", (Throwable)e);
              response.put("code", Integer.valueOf(501));
              message.reply((Object)response);
          }
      }
      );
      
      this.eb.consumer("1.get.dealerlink").handler(message -> {    	  
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          try {
              this.miscQuery = this.mysqlClient.prepareStatement("Select idtbaddealer,name,website from tbaddealer Where active=1 ");
              this.resultSet = this.miscQuery.executeQuery();
              ResultSetMetaData rsMetaData = this.resultSet.getMetaData();
              int numberOfColumns = rsMetaData.getColumnCount();
              while (this.resultSet.next()) {
                  JsonObject result = new JsonObject();
                  for (int i = 1; i <= numberOfColumns; ++i) {
                      result.put(rsMetaData.getColumnLabel(i), this.resultSet.getString(rsMetaData.getColumnLabel(i)));
                  }
                  resultArray.add(result);
              }
              response.put("code", Integer.valueOf(200));
              response.put("result", resultArray);
              message.reply((Object)response);
          }
          catch (Exception e) {
              this.logger.error((Object)"NOTIFY SUPPORT : Error in getting Dealer Link info", (Throwable)e);
              response.put("code", Integer.valueOf(501));
              message.reply((Object)response);
          }
      }
      );
      
      this.eb.consumer("1.get.financierlink").handler(message -> {    	  
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          try {
              this.miscQuery = this.mysqlClient.prepareStatement("Select idtbadfinancier,name,website from tbadfinancier Where active=1 ");
              this.resultSet = this.miscQuery.executeQuery();
              ResultSetMetaData rsMetaData = this.resultSet.getMetaData();
              int numberOfColumns = rsMetaData.getColumnCount();
              while (this.resultSet.next()) {
                  JsonObject result = new JsonObject();
                  for (int i = 1; i <= numberOfColumns; ++i) {
                      result.put(rsMetaData.getColumnLabel(i), this.resultSet.getString(rsMetaData.getColumnLabel(i)));
                  }
                  resultArray.add(result);
              }
              response.put("code", Integer.valueOf(200));
              response.put("result", resultArray);
              message.reply((Object)response);
          }
          catch (Exception e) {
              this.logger.error((Object)"NOTIFY SUPPORT : Error in getting Financier Link info", (Throwable)e);
              response.put("code", Integer.valueOf(501));
              message.reply((Object)response);
          }
      }
      );
      
      this.eb.consumer("1.get.insurerlink").handler(message -> {    	  
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          try {
              this.miscQuery = this.mysqlClient.prepareStatement("Select idtbadinsurer,name,website from tbadinsurer Where active=1 ");
              this.resultSet = this.miscQuery.executeQuery();
              ResultSetMetaData rsMetaData = this.resultSet.getMetaData();
              int numberOfColumns = rsMetaData.getColumnCount();
              while (this.resultSet.next()) {
                  JsonObject result = new JsonObject();
                  for (int i = 1; i <= numberOfColumns; ++i) {
                      result.put(rsMetaData.getColumnLabel(i), this.resultSet.getString(rsMetaData.getColumnLabel(i)));
                  }
                  resultArray.add(result);
              }
              response.put("code", Integer.valueOf(200));
              response.put("result", resultArray);
              message.reply((Object)response);
          }
          catch (Exception e) {
              this.logger.error((Object)"NOTIFY SUPPORT : Error in getting Insurer Link info", (Throwable)e);
              response.put("code", Integer.valueOf(501));
              message.reply((Object)response);
          }
      }
      );
      
      this.eb.consumer("1.get.product").handler(message -> {    	  
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          try {
              this.miscQuery = this.mysqlClient.prepareStatement("Select idtbadproduct,idtbadbrand,model,price from tbadproduct Where active=1 ");
              this.resultSet = this.miscQuery.executeQuery();
              ResultSetMetaData rsMetaData = this.resultSet.getMetaData();
              int numberOfColumns = rsMetaData.getColumnCount();
              while (this.resultSet.next()) {
                  JsonObject result = new JsonObject();
                  for (int i = 1; i <= numberOfColumns; ++i) {
                      result.put(rsMetaData.getColumnLabel(i), this.resultSet.getString(rsMetaData.getColumnLabel(i)));
                  }
                  resultArray.add(result);
              }
              response.put("code", Integer.valueOf(200));
              response.put("result", resultArray);
              message.reply((Object)response);
          }
          catch (Exception e) {
              this.logger.error((Object)"NOTIFY SUPPORT : Error in getting Product info", (Throwable)e);
              response.put("code", Integer.valueOf(501));
              message.reply((Object)response);
          }
      }
      );
      
      this.eb.consumer("1.get.recommended").handler(message -> {    	  
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          try {
              this.miscQuery = this.mysqlClient.prepareStatement("Select idtbadproduct,idtbadbrand,model,price from tbadproduct Where active=1 ");
              this.resultSet = this.miscQuery.executeQuery();
              ResultSetMetaData rsMetaData = this.resultSet.getMetaData();
              int numberOfColumns = rsMetaData.getColumnCount();
              while (this.resultSet.next()) {
                  JsonObject result = new JsonObject();
                  for (int i = 1; i <= numberOfColumns; ++i) {
                      result.put(rsMetaData.getColumnLabel(i), this.resultSet.getString(rsMetaData.getColumnLabel(i)));
                  }
                  resultArray.add(result);
              }
              response.put("code", Integer.valueOf(200));
              response.put("result", resultArray);
              message.reply((Object)response);
          }
          catch (Exception e) {
              this.logger.error((Object)"NOTIFY SUPPORT : Error in getting Product info", (Throwable)e);
              response.put("code", Integer.valueOf(501));
              message.reply((Object)response);
          }
      }
      );
      
      this.eb.consumer("1.get.features").handler(message -> {    	  
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          try {
              this.miscQuery = this.mysqlClient.prepareStatement("Select idtbadproduct,idtbadbrand,model,price from tbadproduct Where active=1 ");
              this.resultSet = this.miscQuery.executeQuery();
              ResultSetMetaData rsMetaData = this.resultSet.getMetaData();
              int numberOfColumns = rsMetaData.getColumnCount();
              while (this.resultSet.next()) {
                  JsonObject result = new JsonObject();
                  for (int i = 1; i <= numberOfColumns; ++i) {
                      result.put(rsMetaData.getColumnLabel(i), this.resultSet.getString(rsMetaData.getColumnLabel(i)));
                  }
                  resultArray.add(result);
              }
              response.put("code", Integer.valueOf(200));
              response.put("result", resultArray);
              message.reply((Object)response);
          }
          catch (Exception e) {
              this.logger.error((Object)"NOTIFY SUPPORT : Error in getting Product info", (Throwable)e);
              response.put("code", Integer.valueOf(501));
              message.reply((Object)response);
          }
      }
      );
      
   }
}