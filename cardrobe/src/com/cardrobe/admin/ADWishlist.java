package com.cardrobe.admin;

import com.cardrobe.admin.BaseAbstractVerticle;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ADWishlist extends BaseAbstractVerticle {
	private PreparedStatement miscQuery = null;
	private ResultSet resultSet = null;
	private static final String CODE = "code";
	private static final String RESULT = "result";
	private static final String USER_ID = "userID";
	private Integer userID;
	private Integer wishlistID;
	private static final String BANK_ID = "id";
	private static final String BANK_CODE = "cod";
	private static final String BANK_NAME = "bnam";
	private static final String BRANCH_NAME = "bran";
	private static final String BRANCH_CODE = "bcod";
	private static final String ADDRESS = "addr";
	private static final String ACTIVE = "act";
	
	public void stop() {
	}

	public void start() {
      super.start();
      this.logger = LoggerFactory.getLogger(this.getClass());
      
      this.eb.consumer("1.get.adwishlist").handler((message) -> {
          JsonObject messageBody = new JsonObject((String)message.body());
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          this.userID = messageBody.getInteger("userID");

          try {
             this.mysqlClient.setCatalog(messageBody.getString("userdb"));
             this.miscQuery = this.mysqlClient.prepareStatement("Select idtbadwishlist,idtbadproduct FROM tbadwishlist Where idtbaduser=?");
             this.miscQuery.setInt(1, this.userID.intValue());
             this.resultSet = this.miscQuery.executeQuery();
             ResultSetMetaData e = this.resultSet.getMetaData();
             int numberOfColumns = e.getColumnCount();

             while(this.resultSet.next()) {
                JsonObject result = new JsonObject();

                for(int i = 1; i <= numberOfColumns; ++i) {
                   result.put(e.getColumnLabel(i), this.resultSet.getString(e.getColumnLabel(i)));
                }

                resultArray.add(result);
             }

             this.miscQuery.close();
             response.put("code", Integer.valueOf(200));
             response.put("result", resultArray);
             message.reply(response);
          } catch (Exception arg8) {
             this.logger.error("NOTIFY SUPPORT : Error in wishlist info", arg8);
             response.put("code", Integer.valueOf(501));
             message.reply(response);
          }

       });
      
      this.eb.consumer("1.post.adwishlist").handler((message) -> {
          JsonObject messageBody = new JsonObject((String)message.body());
          JsonObject response = new JsonObject();
          this.userID = messageBody.getInteger("userID");

          try {
             this.mysqlClient.setCatalog(messageBody.getString("userdb"));
             this.miscQuery = this.mysqlClient.prepareStatement("insert into tbadwishlist (idtbaduser,idtbadproduct) values (?,?)");
             this.miscQuery.setInt(1, this.userID.intValue());
             this.miscQuery.setInt(2, messageBody.getInteger("idtbadproduct").intValue());
             this.miscQuery.execute();
             this.logger.info("Inserted wishlist item");
             this.miscQuery.close();
             response.put("code", Integer.valueOf(200));
             message.reply(response);
          } catch (SQLException arg4) {
             this.logger.error("NOTIFY SUPPORT : Error in inserting wishlist record", arg4);
             response.put("code", Integer.valueOf(501));
             message.reply(response);
          }

       });
      
      this.eb.consumer("1.delete.adwishlist").handler((message) -> {
          JsonObject messageBody = new JsonObject((String)message.body());
          JsonObject response = new JsonObject();
          JsonObject pathParams = messageBody.getJsonObject("path_params");
          this.wishlistID = Integer.valueOf(Integer.parseInt(pathParams.getString("param_0")));
          this.userID = messageBody.getInteger("userID");
          if(this.wishlistID != null && this.wishlistID.intValue() != 0) {
             try {
                this.mysqlClient.setCatalog(messageBody.getString("userdb"));
                this.miscQuery = this.mysqlClient.prepareStatement("delete from tbadwishlist where idtbadwishlist=?");
                this.miscQuery.setInt(1, this.wishlistID.intValue());
                this.miscQuery.execute();
                this.miscQuery.close();
                response.put("code", Integer.valueOf(200));
                message.reply(response);
             } catch (SQLException arg5) {
                this.logger.error("NOTIFY SUPPORT : Error in deleting wishlist record", arg5);
                response.put("code", Integer.valueOf(501));
                message.reply(response);
             }

          } else {
             response.put("code", Integer.valueOf(623));
             message.reply(response);
          }
       });
      
   }
}
