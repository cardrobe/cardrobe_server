package com.cardrobe.admin;

import java.util.Locale;
import java.util.ResourceBundle;

public class Message {
	private ResourceBundle messages;

	public Message(String loc) {
		this.messages = ResourceBundle.getBundle("i18n.MessagesBundle",
				new Locale(loc));
	}

	public String getMessage(String msgKey) {
		return this.messages.getString(msgKey);
	}
}