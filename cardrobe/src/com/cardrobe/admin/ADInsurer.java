package com.cardrobe.admin;

import com.cardrobe.admin.BaseAbstractVerticle;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ADInsurer extends BaseAbstractVerticle {
	private PreparedStatement miscQuery = null;
	private ResultSet resultSet = null;
	private static final String CODE = "code";
	private static final String RESULT = "result";
	private static final String USER_ID = "userID";
	private Integer userID;
	private static final String MODULE_ID = "moid";

	public void stop() {
	}

	public void start() {
      super.start();
      this.logger = LoggerFactory.getLogger(this.getClass());           
      this.eb.consumer("1.get.insurerdetails").handler(message -> {    	  
          JsonObject response = new JsonObject();
          JsonArray resultArray = new JsonArray();
          try {
              this.miscQuery = this.mysqlClient.prepareStatement("Select idtbadinsurer,name,website,address,email,phone from tbadinsurer Where active=1 ");
              this.resultSet = this.miscQuery.executeQuery();
              ResultSetMetaData rsMetaData = this.resultSet.getMetaData();
              int numberOfColumns = rsMetaData.getColumnCount();
              while (this.resultSet.next()) {
                  JsonObject result = new JsonObject();
                  for (int i = 1; i <= numberOfColumns; ++i) {
                      result.put(rsMetaData.getColumnLabel(i), this.resultSet.getString(rsMetaData.getColumnLabel(i)));
                  }
                  resultArray.add(result);
              }
              response.put("code", Integer.valueOf(200));
              response.put("result", resultArray);
              message.reply((Object)response);
          }
          catch (Exception e) {
              this.logger.error((Object)"NOTIFY SUPPORT : Error in getting Insurer Details info", (Throwable)e);
              response.put("code", Integer.valueOf(501));
              message.reply((Object)response);
          }
      }
      );
      
   }
}
