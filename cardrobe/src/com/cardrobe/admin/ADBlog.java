package com.cardrobe.admin;

import com.cardrobe.admin.BaseAbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class ADBlog extends BaseAbstractVerticle {
	private PreparedStatement blogSelectSingle = null;
	private PreparedStatement blogSearch = null;
	private PreparedStatement blogInsert = null;
	private ResultSet resultSet = null;
	private static final String CODE = "code";
	private static final String RESULT = "result";
	private static final String USER_ID = "userID";
	private Integer userID;
	private Integer blogID;
	private static final String BANK_ID = "id";
	private static final String BANK_CODE = "cod";
	private static final String BANK_NAME = "bnam";
	private static final String BRANCH_NAME = "bran";
	private static final String BRANCH_CODE = "bcod";
	private static final String ADDRESS = "addr";
	private static final String ACTIVE = "act";
	
	public void stop() {
	}

	public void start() {
      super.start();
      this.logger = LoggerFactory.getLogger(this.getClass());
      this.eb.consumer("1.get.adblog").handler((message) -> {
         JsonObject messageBody = new JsonObject((String)message.body());
         JsonObject response = new JsonObject();
         JsonObject pathParams = messageBody.getJsonObject("path_params");
         this.blogID = null;
         if(pathParams.getString("param_0") != null) {
            this.blogID = Integer.valueOf(Integer.parseInt(pathParams.getString("param_0")));
         }

         JsonArray resultArray = new JsonArray();

         try {
            this.mysqlClient.setCatalog(messageBody.getString("userdb"));
            this.blogSelectSingle = this.mysqlClient.prepareStatement("SELECT idtbadblog,tittle,imageurl,description,author,date,rating,CASE WHEN active =1 Then \'ACTIVE\' ELSE \'INACTIVE\' END act FROM tbadblog WHERE  idtbadblog=?");
            this.blogSelectSingle.setInt(1, this.blogID.intValue());
            this.resultSet = this.blogSelectSingle.executeQuery();
            ResultSetMetaData e = this.resultSet.getMetaData();
            int numberOfColumns = e.getColumnCount();

            while(this.resultSet.next()) {
               JsonObject result = new JsonObject();

               for(int i = 1; i <= numberOfColumns; ++i) {
                  result.put(e.getColumnLabel(i), this.resultSet.getString(e.getColumnLabel(i)));
               }

               resultArray.add(result);
            }

            this.blogSelectSingle.close();
            response.put("code", Integer.valueOf(200));
            response.put("result", resultArray);
            message.reply(response);
         } catch (Exception arg9) {
            this.logger.error("NOTIFY SUPPORT : Error in getting BLOG info", arg9);
            response.put("code", Integer.valueOf(501));
            message.reply(response);
         }

      });
      this.eb.consumer("1.get.adblogsearch").handler((message) -> {
         JsonObject messageBody = new JsonObject((String)message.body());
         JsonObject response = new JsonObject();
         this.blogID = null;
         JsonArray resultArray = new JsonArray();

         try {
            this.mysqlClient.setCatalog(messageBody.getString("userdb"));
            this.blogSearch = this.mysqlClient.prepareStatement("select * from tbadblog limit ?");
            this.blogSearch.setInt(1, this.pageSize);
            this.resultSet = this.blogSearch.executeQuery();
            ResultSetMetaData e = this.resultSet.getMetaData();
            int numberOfColumns = e.getColumnCount();

            while(this.resultSet.next()) {
               JsonObject result = new JsonObject();

               for(int i = 1; i <= numberOfColumns; ++i) {
                  result.put(e.getColumnLabel(i), this.resultSet.getString(e.getColumnLabel(i)));
               }

               resultArray.add(result);
            }

            this.blogSearch.close();
            response.put("code", Integer.valueOf(200));
            response.put("result", resultArray);
            message.reply(response);
         } catch (Exception arg8) {
            this.logger.error("NOTIFY SUPPORT : Error in getting BLOG info", arg8);
            response.put("code", Integer.valueOf(501));
            message.reply(response);
         }

      });

      this.eb.consumer("1.post.adblogcomment").handler((message) -> {
         JsonObject messageBody = new JsonObject((String)message.body());
         JsonObject response = new JsonObject();
         this.userID = messageBody.getInteger("userID");
         ResultSet rsreturnkeyset = null;

         try {
            this.mysqlClient.setCatalog(messageBody.getString("userdb"));
            this.blogInsert = this.mysqlClient.prepareStatement("INSERT INTO tbadcomment (idtbadblog,comment,cauthor)  VALUES (?,?,?)", 1);
            this.blogInsert.setString(1, messageBody.getString("idtbadblog"));
            this.blogInsert.setString(2, messageBody.getString("comment"));
            this.blogInsert.setString(3, messageBody.getString("cauthor"));
            this.blogInsert.execute();
            rsreturnkeyset = this.blogInsert.getGeneratedKeys();
            if(rsreturnkeyset.next()) {
               this.blogID = Integer.valueOf(rsreturnkeyset.getInt(1));
            }

            rsreturnkeyset.close();
            this.logger.info("Inserted BANK record with BANK ID:" + this.blogID);
            this.blogInsert.close();
            response.put("code", Integer.valueOf(200));
            message.reply(response);
         } catch (SQLException arg5) {
            this.logger.error("NOTIFY SUPPORT : Error while inserting BANK record", arg5);
            response.put("code", Integer.valueOf(501));
            message.reply(response);
         }

      });

   }
}
